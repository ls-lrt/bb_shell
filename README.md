Small shell to interact with the sphero
=======================================

Setup and run
-------------

You should clone sphero.js project in the parent directory:

```
git clone https://github.com/orbotix/sphero.js.git 
```

Then go to the bb_shell directory and run:

```
PORT="aa:bb:cc:dd:ee:ff" node shell.js
```
