var readline = require('readline');
var a = require("./a.js").a;

var myPrompt = "test > ";

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true,
    prompt: myPrompt
});

var tests = {
    process_line: {
	help: "test eval",
	fun : function (line){
	    var txt = "function(a) { console.log('txt :' + a); }";
	    //var fun = function (a) {console.log("function txt : " + a)};
	    var fun = eval("(" + txt + ")");
	    fun(line);
	}
    },

    test_re: {
	help: "Test regexp",
	fun : function (){
	    var str = "function (callback, lol)";
	    var re = new RegExp(/function\s*\((.*kikoo)\)/, "gi");
	    var match = re.exec(str);
	    console.log(match.length);
	    console.log(match);
	}
    },

    hasBuiltinSyntaxe: {
	help: "",
	fun: function (cmd) {
	    return (typeof cmd === "string" && cmd != null && cmd[0] === "!");
	}
    },

    test_hasBuiltinSyntaxe: {
	help: "",
	fun : function (){
	    ["toto", "!toto", null, "", [], {} ].forEach(function(a){
		console.log("("+a+") : " + hasBuiltinSyntaxe(a));
	    });
	}
    },

    listGlobals: {
	help: "",
	fun : function () {
	    for (var k in global) { console.log(k); }
	}
    },

    testSplit: {
	help: "",
	fun : function () {
	    var str = "toto tata titi";
	    function a (foo, bar) {
		console.log("[a] foo = " + foo);
		console.log("[a] bar = " + bar);
	    };
	    a(str.split(" "));
	}
    },

    copyMe: {
	help: "example test function",
	fun : function () {
	}
    },

    copyProcess: {
	help: "example test function",
	fun : function () {
	    var a = "toto";
	    var b = a;
	    console.log("a: " + a);
	    console.log("b: " + b);
	    b[0] = "l";
	    console.log("a: " + a);
	    console.log("b: " + b);
	}
    },

    test_a: {
	help: "example test function",
	fun : function () {
	    a(function(){rl.removeListener('line', handle_line);}, rl, function(){rl.on('line', handle_line);rl.clearLine(process.stdin);}); 
	}
    },

    testBind: {
	help: "bind",
	fun : function () {
	    var a = 42;
	    function changeAndPrintA(x) {
		a += x;
		console.log(a);
	    }

	    var add1 = changeAndPrintA.bind(null, 1);
	    var rm1 = changeAndPrintA.bind(null, -1);
	    add1();
	    rm1();
	}
    },

}

function needHelp(str) {
    return (typeof str === "string" && str != null && (str[0] === "?" || str[str.length - 1] === "?"));
}

function print(str)   { process.stdout.write(str) }
function prompt(inStr) {
    var str = (typeof inStr === "string") ? inStr : "\n" + myPrompt;
    print(str);
}

function displayTests() {
    for (k in tests) {
	console.log("\t" + k + " :\n\t\t" + tests[k].help);
    }
    prompt();
}

function handle_line(line){
    var cmd = line;
    var h = needHelp(line);
    if (h) { cmd = cmd.replace(/\?/g, ""); }
    if (h && cmd === "") { return displayTests(); }
    if (!h && cmd === "") { return prompt(); }
    if (cmd in tests) {
	if (h)
	    console.log(tests[cmd].help);
	else
	    tests[cmd].fun();
    } else {
	console.log("'" + cmd + "' not found type ? to display all available tests.");
    }
    return prompt();
}

rl.on('line', handle_line);
prompt();
