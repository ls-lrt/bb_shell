exports.a = function(init, orb, exit) {
    var keypress = require("keypress");

    function handle(ch, key) {
	if (key.name === "q") {
	    process.stdin.removeListener("keypress", handle);
	    exit();
	    console.log("Bye");
	}
    }

    init();

    keypress(process.stdin);
    process.stdin.on("keypress", handle);
    console.log("Press q to exit");
    process.stdin.setRawMode(true);
    process.stdin.resume();
}
