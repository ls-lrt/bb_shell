module.exports = function(grunt) {
    grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	jshint: {
	    files: ['Gruntfile.js', 'shell.js', 'module/*.js'],
	    options: {
		globals: {
		    console: true,
		    process: true,
		    require: true,
		}
	    },
	},
	watch: {
	    files: ['<%= jshint.files %>'],
	    tasks: ['jshint']
	}
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('check', ['jshint']);
    grunt.registerTask('default', ['jshint']);
};
