exports.keyboard = function(init, orb, exit) {
    var keypress = require("keypress");
    var speed = 60,
	angle = 0,
	delta = 10;
    var absolute = false,
	calibrating = false;
    var stop = orb.roll.bind(orb, 0, 0),
	roll = orb.roll.bind(orb, speed);

    function setSpeed(newSpeed) {
	speed = (newSpeed > 200) ? 200 : (newSpeed < 0) ? 0 : newSpeed;
	console.log(speed);
	roll = orb.roll.bind(orb, speed);
    }

    function setDelta(newDelta) {
	delta = (newDelta > 20) ? 20 : (newDelta < 0) ? 0 : newDelta;
	console.log(delta);
    }

    function toggleAbsoluteMode() {
	absolute = !absolute;
	console.log("Absolute mode : " + (absolute ? "enabled" : "disabled"));
    }

    function toggleCalibration() {
	if (calibrating)
	    orb.finishCalibration();
	else
	    orb.startCalibration();
	calibrating = !calibrating;
	console.log((calibrating ? "Starting" : "finishing") + " calibration");
    }

    function quit() {
	stop();
	process.stdin.removeListener("keypress", handle);
	exit();
	console.log("Bye");
    }

    function help() {
	console.log("\nControls:");
	for (var c in controlKeys) { console.log("\t" + c + " : " + controlKeys[c].help); }
	console.log("Actions:");
	for (var a in actionKeys) { console.log("\t" + a + " : " + actionKeys[a].help); }
    }

    var controlKeys = {
	"up" : {
	    help:  "roll up",
	    action: function() {angle = absolute ? 0 : angle;} },
	"down": {
	    help:  "roll down",
	    action: function() {angle = absolute ? 180 : (angle + 180) % 360;}},
	"right" : {
	    help:  "turn right",
	    action: function() {angle = absolute ? 90  : (angle + delta) % 360;}},
	"left": {
	    help:  "turn left",
	    action: function() {angle = absolute ? 270 : (angle < delta) ? (360 - delta) : angle - delta;}},
	"space": {
	    help:  "stop",
	    action: function() {return true;}},
	"p": {
	    help: "increase speed (+10%)",
	    action: function() {setSpeed(speed + 10); return absolute;}},
	"h": {
	    help: "decrease speed (-10%)",
	    action: function() {setSpeed(speed - 10); return absolute;}},
    };

    var actionKeys = {
	"q": {
	    help: "quit",
	    action: quit},
	"a": {
	    help: "toggle absolute mode",
	    action: toggleAbsoluteMode},
	"c": {
	    help: "start/stop calibration",
	    action: toggleCalibration},
	"o": {
	    help: "increase control sensitivity (+1°)",
	    action: function() { setDelta(delta + 1);}},
	"l" : {
	    help: "decrease control sensitivity (-1°)",
	    action: function() { setDelta(delta - 1);}},
	"h" : {
	    help: "display help",
	    action: help},
    };

    function handle(ch, key)  {
	if (key.name in actionKeys)  { return actionKeys[key.name].action(); }
	if (key.name in controlKeys) { if (controlKeys[key.name].action()) { stop(); } else { roll(angle); } }
    }

    init();

    keypress(process.stdin);
    process.stdin.on("keypress", handle);
    console.log("Starting keyboard control, press q to quit or h for help.");
    process.stdin.setRawMode(true);
    process.stdin.resume();
};
