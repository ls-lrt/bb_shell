exports.disco = function(init, orb, exit, swagHandle) {
    var keypress = require("keypress");
    var color, handle,
	swaggy = false,
	luminance = 0,
	delta = 300,
	dancing = true;

    function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
    }

    function get_random_rgb() {
	return {red: getRandomInt(0,255), green: getRandomInt(0,255), blue: getRandomInt(0,255)};
    }

    function p_color() {
	if (typeof color === "undefined") { return; }
	var str = "color: { ";
	for (var k in color) {
	    str += k + " : " + color[k] + " ";
	}
	str += "}";
	console.log(str);
    }

    function swag(t) {
	return setInterval(function() {
	    color = get_random_rgb();
	    orb.color(color, luminance);
	}, t);
    }

    function quit() {
	process.stdin.removeListener("keypress", handleKeypress);
	if (!swaggy) {
	    orb.color({ red: 0, green: 0, blue: 0 });
	    clearInterval(handle);
	    exit();
	} else {
	    exit(handle);
	}
	console.log("Bye");
    }

    function help() {
	console.log("\nActions:");
	for (var k in actionKeys) { console.log("\t" + k + " : " + actionKeys[k].help); }
    }

    function pauseStart() {
	if (dancing)
	    clearInterval(handle);
	else
	    handle = swag(delta);
	dancing = !dancing;
    }

    function changeDelta(x) {
	delta += (delta + x ) < 100 ? 0 : x;
	console.log(delta);
    }

    function changeLuminance(x) {
	luminance += (luminance + x ) < -1.0 ? 0 : (luminance + x) > 1.0 ? 0 : x;
	console.log(luminance);
	pauseStart();
	pauseStart();
    }

    function toggleSwagMode() {
	swaggy = !swaggy;
	if (swaggy)
	    console.log("Man, you're so swag!");
    }

    var actionKeys = {
	"q": {
	    help: "quit",
	    action: quit},
	"h" : {
	    help: "display help",
	    action: help},
	"space" : {
	    help: "pause/start",
	    action: pauseStart},
	"c" : {
	    help: "display current color",
	    action: p_color},
	"s" : {
	    help: "toggle swag mode",
	    action: toggleSwagMode},
	"down" : {
	    help: "decrease luminance by 10%",
	    action: function() { changeLuminance(-0.1); }},
	"left" : {
	    help: "slow down blink time by 100ms",
	    action: function() { changeDelta(100); }},
	"up" : {
	    help: "increase luminance by 10%",
	    action: function() { changeLuminance(0.1); }},
	"right" : {
	    help: "speed up blink time by 100ms",
	    action: function() { changeDelta(-100); }},
    };

    function handleKeypress(ch, key) {
	if (key.name in actionKeys)  { return actionKeys[key.name].action(); }
    }

    init();

    keypress(process.stdin);
    process.stdin.on("keypress", handleKeypress);
    console.log("Starting disco mode, press q to exit");
    process.stdin.setRawMode(true);
    process.stdin.resume();
    if (swagHandle)
	clearInterval(swagHandle);
    handle = swag(delta);
};
