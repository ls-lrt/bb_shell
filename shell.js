var sphero = require("../sphero.js");
var readline = require('readline');
var keyboard = require("./module/keyboard.js").keyboard;
var disco = require("./module/disco.js").disco;
var discoHandle;

var orb = sphero(process.env.PORT);
var connected = false;
var orbKeys = "", hostname = "";
var options = [];

/* return true if the string is one of the element of the array */
String.prototype.isEither = function (array) {
    if (array !== null && typeof array === "object") {
	var this_str = this.toString();
	for (var i in array)
	    if (this_str === array[i]) return true;
    }
    return false;
};

function print(str)   { process.stdout.write(str); }
function printColor(str, code) {process.stdout.write("\x1b[" + code + str + "\x1b[0m"); }
function warning(str) { printColor(str, "38;5;3m"); }
function error(str)   { printColor(str, "38;5;9m"); }

function prompt(inStr) {
    var str = (typeof inStr === "string") ? inStr : "\n" + hostname +" > ";
    print(str);
}

function genericGet (action, cb) {
    orb[action](function dump_response(err, data) {
	if (err) {
	    console.log("error: ", err);
	} else {
	    for (var k in data) {
		console.log(k + " : " + data[k]);
		if (k === "packet") {
		    var p = data.packet;
		    for (var l in p) {
			console.log("\t" + l + " : " + p[l]);
		    }
		}
	    }
	}
	if (typeof cb  === "function") {
	    cb();
	}
    });
}

/* XXX hide this in a module if possible...*/
var userCompleting = false;
var context = {
    action : "",
    params : [],
    userInputs : [],
    index : 0
};

function userComplete(obj, action) {
    var reParams = new RegExp(/function\s*\((.*)\)/, "gi");
    var match = reParams.exec(obj);
    if (match === null) {
	error("Fail to guess what " + action + " needs");
	return;
    }
    context.index = 0;
    context.action = action;
    context.params = match[1].replace(/\s/g, '').split(",");
    context.userInputs = [];
    userCompleting = true;
    prompt(context.params[context.index] + " > ");
}

function complete(line) {
    context.userInputs[context.index] = (line ? line : "null");
    context.index++;
    if (context.index < context.params.length) {
	return  prompt(context.params[context.index] + " > ");
    }
    //exec the function
    userCompleting = false;
    var str = "orb." + context.action + "(" + context.userInputs.join() + ")";
    console.log("running " + str);
    //jshint evil:true
    eval(str);
    return prompt();
}

function printPrototype(obj) {
    console.log("\x1b[38;5;8m" + obj + "\x1b[0m");
    return;
}

function run (action, cb) {
    if (action.match(/^get/))
	return genericGet(action, cb);
    warning("No candidate for these class of functions\n");
    printPrototype(orb[action]);
    return userComplete(orb[action], action);
}

function printBool(bool, cb) {
    print(bool + " : " + orb[bool]);
    if (typeof cb  === "function") {
	cb();
    }
}

var action_dispatcher = {
    "function" : function(action, cb) { run(action, cb); },
    "boolean"  : function(bool, cb)   { printBool(bool, cb); }
};

function builtinQuit() {
    /* TODO: shutdown, sleep, turn off light? */
    print("Bye");
}

function genericInit(){ rl.removeListener('line', processLine); }

function genericExit(){ rl.on('line', processLine); rl.clearLine(process.stdin); prompt(); }

function startKeyboardControl() {
    keyboard(genericInit, orb, genericExit);
}

function discoExit(h) {
    genericExit();
    if (h)
	discoHandle = h;
}

function startDiscoBallMode() {
    disco(genericInit, orb, discoExit, discoHandle);
}

var builtinCmdDispatcher = {
    "quit"    : builtinQuit,
    "control" : startKeyboardControl,
    "disco"   : startDiscoBallMode,
};

function handleMultipleChoices(line) {
    /* [Y/n] */
    if (options.length == 1) {
	if (line.isEither(["", "y", "Y", "yes", "Yes"])) {
	    console.log("running: " + options[0]);
	    return runOrbCmd(options[0]);
	}
	options = [];
	return prompt();
    }
    /* [0-xx] */
    var choice = parseInt(line);
    if (isNaN(choice) || choice >= options.length) {
	/* retry */
	error("Error bad value ");
	options = [];
	return prompt();
    }
    return runOrbCmd(options[choice]);
}

function regexpMatchCmd(line) {
    var re = new RegExp("\\w*" + line + "\\w*", "gi");
    var match = re.exec(orbKeys);
    if (!match) {
	print("Invalid command: " + line);
	return prompt();
    }
    do {
	options.push(match[0]);
    } while ((match = re.exec(orbKeys)));

    /* Display choice(s) */
    var choice_prompt = "\n";
    if (options.length == 1) {
	print("Did you mean " + options[0] + " ?");
	choice_prompt += "[Y/n] > ";
    } else {
	choice_prompt += "[0-" + (options.length - 1) + "] > ";
	print("Invalid command, available commands:");
	var justify = Math.floor(Math.log10(options.length)) + 1;
	var space;
	for (var i in options) {
	    for (var j = i.toString().length, padding = " "; j < justify; j++) padding+=" ";
	    print("\n\t" + i + padding + options[i]);
	}
    }
    return prompt(choice_prompt);
}

function runOrbCmd(cmd) {
    options = [];
    var typeOfCmd = typeof orb[cmd];
    if (!(typeOfCmd in action_dispatcher)) {
	error("Error: " + cmd + " is a " + typeOfCmd);
	return prompt();
    }
    return action_dispatcher[typeOfCmd](cmd, prompt);
}

function hasBuiltinSyntaxe(str) {
    return (typeof str === "string" && str !== null && str[0] === "!");
}

function runBuiltinCmd(builtinCmd) {
    var cmd = builtinCmd.substr(1);
    if (!(cmd in builtinCmdDispatcher)) {
	print(cmd + ": command not found\n");
	return prompt();
    }
    return builtinCmdDispatcher[cmd](prompt);
}

function processLine(line) {

    if (!connected) return console.log("Not yet connected!");

    if (hasBuiltinSyntaxe(line)) return runBuiltinCmd(line);

    if (userCompleting) return complete(line);

    if (options.length) return handleMultipleChoices(line);

    if (!line) return prompt();

    if (line in orb) return runOrbCmd(line);

    /* Last try, help the user */
    return regexpMatchCmd(line);
}

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true
});

rl.on('line', processLine);

process.on('SIGINT', function() {
    if (userCompleting) {
	userCompleting = false;
	return prompt();
    }
    if (options.length) {
	options = [];
	return prompt();
    }
    console.log("Bye");
    process.stdin.pause();
    process.exit();
});

function populateHostname(){
    orb.getBluetoothInfo(function setHostname(err, data) {
	if (err) {
	    console.log("error: ", err);
	} else {
	    hostname = data.name.substr(0,7);
	}
	prompt();
    });
}

function buildListOrbKeys(cb) {
    /* cache available keys on orb object to perform regexp search */
    for (var k in orb) {
	orbKeys += " " + k;
    }
    if (typeof cb === "function") cb();
}

orb.connect(function() {
    buildListOrbKeys(function() {connected = true;});
    populateHostname();
});
